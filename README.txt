
-- SUMMARY --

Source Code is a module for Drupal 7 that defines a Source Code node type. Nodes
of this type are intended to contain source code: their data is composed of a
programming language (from a list of over 200 languages) and the actual lines of
code, as a raw block of plain text. Upon rendering nodes of this type, the
source code is syntax-highlighted using GeSHi.

A typical use of this module is to combine it with Advanced Entity Tokens and
Token Filter to allow the insertion of source code snippets directly into more
descriptive posts.

For a full description of the module, visit the project page:
https://drupal.org/sandbox/krizalys/1951256

-- REQUIREMENTS --

The Source Code module requires the following:
* The libraries module
* The GeSHi library

-- INSTALLATION --

Make sure that the latest stable version of the libraries module is installed on
your Drupal 7 website.

You then have to get the latest version of the GeSHi library and extract it into
your sites/all/libraries folder. If the folder does not exist, create it. The
top-level folder of the library must have the path sites/all/libraries/geshi.
The Source Code module will inform you about installation problems in the
reports panel.

-- CONFIGURATION --

Go to Administration » Modules » Source Code » Configuration
(admin/config/content/source_code), and select:
* which programming languages will be available when creating Source Code
nodes ;
* whether to use HTML classes rather than inline CSS styles while
syntax-highlighting Source Code nodes.

-- REMOVAL --

For technical reasons, the Source Code package is split into two modules. Both
should be uninstalled separately to remove the whole package.

The Source Code module must be disabled and uninstalled first. This will delete
the Source code content type. Existing source code nodes will be preserved
without their programming language and may be deleted manually.

It will then become possible to disable and uninstall the Source Code
Programming Languages module as soon as there are no fields pending for deletion
in the database. Purging fields pending for deletions happens when the cron
runs.

-- KNOWN ISSUES --

When uninstalling the Source Code module, some PHP notices related to the
comment module may appear. This is due to a Drupal core bug and would be fixed
eventually in a future update of Drupal.

However, these notices do not prevent the module from uninstalling and the
Source code content type to be deleted.

-- DEMONSTRATION --

To demonstrate syntax-highlighting capabilities provided by the module, several
sample Source Code nodes are available.

-- CONTACT --

Current maintainers:
* Christophe Vidal (Krizalys) - https://drupal.org/user/2285056

This project has been sponsored by:
* Krizalys
Alternative software and Internet portals
Visit http://www.krizalys.com/ for more information.

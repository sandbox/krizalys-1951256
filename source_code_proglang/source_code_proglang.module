<?php

/**
 * @file
 * The Source Code's proglang module file, which implements the logic for the
 * programming language field.
 */

/**
 * Converts a programming language "machine" name into a "human" name.
 *
 * Conversion is performed from the GeSHi library. For example, the machine name
 * "objc" would return "Objective-C".
 *
 * The full list of supported programming languages is available in GeSHi's
 * documentation/source code.
 *
 * @param string $name
 *   The machine name of the programming language.
 *
 * @return string
 *   The human name of the programming language supplied.
 */
function _source_code_proglang_fullname($name) {
  $lib = libraries_load('geshi');

  // Ensure the GeSHi library is loaded.
  if (empty($lib['loaded'])) {
    drupal_set_message(t('Drupal was unable to load the <a href="!url">GeSHi library</a> for the following reason: %reason.', array(
      '!url'    => 'http://qbnz.com/highlighter/',
      '%reason' => $lib['error'],
    )), 'error');

    // Politely provides the same name as a fallback.
    return $name;
  }

  $geshi = new GeSHi('', $name);
  return $geshi->get_language_name();
}

/**
 * Gets the list of programming languages currently supported.
 *
 * The full list of supported programming languages is available in GeSHi's
 * documentation/source code.
 *
 * @return array
 *   An associative array of programming languages supported. Items are keyed by
 *   machine name and values are objects with the properties:
 *     - path: the path under which the programming language's definition file
 *       was found (e.g. /drupal/sites/all/libraries/geshi/geshi)
 *     - name: the human name of the programming language
 */
function _source_code_proglang_avail() {
  $proglangs = array();
  $lib = libraries_load('geshi');

  // Ensure the GeSHi library is loaded.
  if (empty($lib['loaded'])) {
    drupal_set_message(t('Drupal was unable to load the <a href="!url">GeSHi library</a> for the following reason: %reason.', array(
      '!url'    => 'http://qbnz.com/highlighter/',
      '%reason' => $lib['error'],
    )), 'error');

    // Politely provide an empty list as a fallback.
    return $proglangs;
  }

  $dir = $lib['library path'] . '/geshi';

  foreach (file_scan_directory($dir, '/\.php$/i') as $fileinfo) {
    $name = $fileinfo->name;
    $geshi = new GeSHi('', $name);
    $geshi->set_language_path($dir);
    $fullname = $geshi->get_language_name();
    unset($geshi);

    $proglangs[$name] = (object) array(
      'path' => $dir,
      'name' => $fullname,
    );
  }

  ksort($proglangs);
  return $proglangs;
}

/**
 * Gets the list of programming languages currently enabled.
 *
 * The full list of programming languages that have been defined in the module
 * configuration.
 *
 * @return array
 *   The programming languages enabled, as an array. Items are keyed by name and
 *   the value is the human name of the programming language.
 */
function _source_code_proglang_enabled() {
  return array_map('_source_code_proglang_fullname', variable_get('source_code_proglang_enabled', array()));
}

/**
 * Implements hook_field_widget_info_alter().
 */
function source_code_proglang_field_widget_info_alter(&$info) {
  $info['options_select']['field types'][] = 'proglang';
  $info['options_buttons']['field types'][] = 'proglang';
}

/**
 * Implements hook_options_list().
 *
 * Generates an option list of programming languages enabled.
 */
function source_code_proglang_options_list($field, $instance, $entity_type, $entity) {
  return _source_code_proglang_enabled();
}

/**
 * Implements hook_field_is_empty().
 *
 * Checks wheter the programming language field is empty.
 */
function source_code_proglang_field_is_empty($item, $field) {
  return empty($item['proglang']) && (string) $item['proglang'] !== '0';
}

/**
 * Implements hook_field_widget_validate().
 *
 * Validates the value of the proglang field.
 */
function source_code_proglang_field_widget_validate($element, &$form_state) {
  options_field_widget_validate($element, $form_state);
}

/**
 * Implements hook_field_formatter_info().
 */
function source_code_proglang_field_formatter_info() {
  return array(
    'plain' => array(
      'label'       => t('Plain text'),
      'description' => t('Plain text'),
      'field types' => array('proglang'),
      'settings'    => array(),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function source_code_proglang_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'plain':
      foreach ($items as $delta => $item) {
        $proglangs = _source_code_proglang_enabled();

        $element[$delta] = array(
          '#type'   => 'markup',
          '#markup' => check_plain($proglangs[$item['proglang']]),
        );
      }

      break;
  }

  return $element;
}

/**
 * Implements hook_field_info().
 */
function source_code_proglang_field_info() {
  return array(
    'proglang' => array(
      'label'             => t('Programming language'),
      'description'       => t('Programming language'),
      'settings'          => array(),
      'instance_settings' => array(),
      'default_widget'    => 'options_select',
      'default_formatter' => 'plain',
      'no_ui'             => FALSE,
    ),
  );
}
